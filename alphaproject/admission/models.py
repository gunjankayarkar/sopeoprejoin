from django.db import models

# Create your models here.
#class Admission(models.Model):
#    title = models.CharField(max_length = 200)
#    description = models.TextField()

class Patientvisit(models.Model):
    visitid = models.AutoField(primary_key=True)
    patientid = models.BigIntegerField(null=True, blank=True)
    patname = models.CharField(max_length=100, blank=True, null=True)
    age = models.IntegerField(blank=True, null=True)
