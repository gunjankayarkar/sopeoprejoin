from django.urls import path, include
from .views import *

urlpatterns = [
    path('', home, name='home'),
    path('page1/', page1, name='page1'),
    path('dashboard/', dashboard, name='dashboard'),
    #path('page1', page1, name='page1'),
]

app_name = 'admission'